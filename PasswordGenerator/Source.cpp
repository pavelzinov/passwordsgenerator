#include <iostream>
#include "GRandom.h"
#include <vector>

using green3d::GRandom;

int main(int argc, char** argv)
{
  std::vector<std::string> characters;
  characters.push_back("!#$@%:&?*-+=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
  characters.push_back("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
  characters.push_back("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
  characters.push_back("#@%:&?*-+=2345679ACDEFGHJKLMNPQRTUVWXYZabcdefghijkmnpqrtuvwxyz");

  std::cout << "Enter the length of passwords: ";
  int password_length = 0;
  std::cin >> password_length;

  int password_type = 0;
  do 
  {
    std::cout << "Enter the type of passwords (0 - all characters, 1 - alphabetical, 2 - alphabetical with numbers, 3 - all characters without confusing ones like '1', 'l', '!' and so on: ";
    std::cin >> password_type;
    if (password_type > characters.size() - 1 && password_type < 0)
    {
      std::cout << "Sorry, you have entered wrong password type, try again.\n";
    }
  } 
  while (password_type > characters.size() - 1 || password_type < 0);

  std::cout << "Enter the number of passwords: ";
  int passwords_count = 0;
  std::cin >> passwords_count;
  
  GRandom rng;
  std::cout << "\nGenerated passwords:\n";
  for (int i = 0; i < passwords_count; ++i)
  {
    for (int j = 0; j < password_length; ++j)
    {
      std::cout << characters[password_type][rng.GenInt(characters[password_type].size() - 1)];
    }
    std::cout << std::endl;
  }

  std::cout << "Press ENTER to exit...";

  std::cin.get();
  std::cin.get();

  return 0;
}